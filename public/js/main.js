$('#editModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var id = button.data('id'); // Extract info from data-* attributes
  
  var name = button.data('name');
  var description = button.data('description');
  var number = button.data('number');
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.modal-title').text('Edit number' );
  modal.find('#id').val(id);
  modal.find('#name').val(name);
  modal.find('#description').val(description);
  modal.find('#number').val(number);
});

$('#deleteModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var id = button.data('id'); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.modal-title').text('You sure want to delete number?' );
  modal.find('#id').val(id);
  modal.find('#send').click(function () {//AJAX FUNCTION FOR DELETE
      var data = {id:id};
      $.ajax({
          data:data,
          url: $('.navbar-brand').attr('href')+'/del',
          type:'GET',
          success:function(responce) {
              if (responce['answer']== 'ok'){
                $('#obj'+id).remove();
                console.log('ok');
                }
          }
          });
  });
});

//validation
$( "#number" ).change(function(){
    var re = /^\d[\d\(\)\ -]{4,14}\d$/;//reg exp phone number
    var number = $( this ).val();
    var valid = re.test(number);
    if  (valid == false) {
        $( "#submitButton").attr('disabled',true);//disable send if not valid
    }
    else {
        $( "#submitButton").attr('disabled',false);
    }
    
});

$( "#name" ).change(function(){// reg exp for 
    var re = /^[a-zA-ZА-Яа-я\s]*$/;// spaces and letters (latin or cyrillic)
    var name = $( this ).val();
    var valid = re.test(name);
    console.log(valid);
    if (name.length <=50 && valid == true){
        $( "#submitButton").attr('disabled',false);//disable send if not valid
    }
    else {
        $( "#submitButton").attr('disabled',true);
    }
    });
 
$( "#description" ).change(function(){//all chars less than 255 length
    var description = $( this ).val();
    if (description.length <=255 ) {
        $( "#submitButton").attr('disabled',false);//disable send if not valid
    }
    else {
        $( "#submitButton").attr('disabled',true);
    }
});