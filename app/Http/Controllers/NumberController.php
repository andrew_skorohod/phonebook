<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Number;

class NumberController extends Controller
{
    
    public function index(Request $request) {//show all
        $numbers = Number::orderBy('created_at', 'desc')->get();
        return view('home',[
            'numbers'=>$numbers,   
        ]);
    }
    
    public function store(Request $request) {//create and edit
        $id = $request->id;
        if ($id > 0) {//edit
            $obj = Number::where('id',$id)->first();
            $obj->name = $request->name;
            $obj->number = $request->number;
            $obj->description = $request->description;
            $obj->save();
        }
        else { 
            $obj = new Number();
            $obj->name = $request->name;
            $obj->number = $request->number;
            $obj->description = $request->description;
            $obj->save();
        }
        return redirect("/");
        
    }
    
    
    public function delete(request $request) {
        $id = $request->id;
        $obj = Number::where ('id',$id)->delete();
        return response()->json(['answer' => 'ok',]);
    }
}

