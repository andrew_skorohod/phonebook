@extends('layouts.app')

@section('content')
    <div>
    @include('common.errors')
    </div>

<!--modal for add-->
<a class="btn btn-lg btn-primary"  role="button" data-toggle="modal"
             data-target="#createObj" style="margin-bottom:15px;">Add Number</a>
             
<div class="modal fade" id="createObj" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action=" {{ url('store') }}" enctype="multipart/form-data">{{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Add number</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="name"  class="control-label" >Name: </label>
            <input type="text" class="form-control" name="name" id="name" required>
          </div>
          <div class="form-group">
            <label for="numer" class="control-label">Number :</label>
            <input class="form-control" name="number" id="number" required>
          </div>
          <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <textarea class="form-control" name="description" id="description" required></textarea>
          </div>          

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input class="btn btn-md btn-info" type="submit" value="Submit" id="submitButton">
      </div>
        </form>
    </div>
  </div>
</div>

<!--modal for edit-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="POST" action="{{ url('store') }}" enctype="multipart/form-data">{{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
      </div>
      <div class="modal-body">
          <div class="form-group" hidden>
            <label for="name"  class="control-label" >id </label>
            <input type="text" class="form-control" name="id" id="id" required>
          </div>          
          <div class="form-group">
            <label for="name"  class="control-label" >Name: </label>
            <input type="text" class="form-control" name="name" id="name" required>
          </div>
          <div class="form-group">
            <label for="number" class="control-label">Number :</label>
            <input class="form-control" name="number" id="number" required>
          </div>
          <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <textarea class="form-control" name="description" id="description" required></textarea>
          </div>          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input class="btn btn-md btn-info" type="submit" value="Submit" id="submitButton">
      </div>
            </form>
    </div>
  </div>
</div>

<!--modal for delete-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="GET" action="{{ url('delete') }}" enctype="multipart/form-data">{{ csrf_field() }}
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Delete number</h4>
      </div>
      <div class="modal-body" hidden>
          <div class="form-group">
            <label for="name"  class="control-label" >id</label>
            <input type="text" class="form-control" name="id" id="id">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="send" >Delete</button>
      </div>
            </form>
    </div>
  </div>
</div>

<table class="table table-bordered sortable">
    
    <thead>
        <tr>
            <th>Id</th>
            <th>Date</th>
            <th>Name</th>
            <th>Number</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($numbers as $number)
        <tr id="obj{{ $number->id }}">
            <td >{{ $number->id }}</td>
            <td>{{ $number->created_at }}</td>
            <td>{{ $number->name }}</td>
            <td>{{ $number->number }}</td>
            <td>{{ $number->description }}</td>
            <td>
                <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#editModal" data-id="{{ $number->id }}" data-name="{{ $number->name }}" data-number="{{ $number->number }}" data-description="{{ $number->description }}">
                    Edit 
                </button>
            </td>
            <td><a class="btn btn-md btn-danger"  id='del{{ $number->id }}'  role="button" data-toggle="modal" data-target="#deleteModal" data-id="{{ $number->id }}" >
                    Delete
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
    
    
@endsection